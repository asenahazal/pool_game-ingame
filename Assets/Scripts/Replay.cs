﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Replay : MonoBehaviour
{

    List<PointInTime> pointsInTime;
    public bool isRewinding = false;
    public Rigidbody rb;
    private PlayerController player;
    //public AudioClip ReplayVoice;



    void Start()
    {
       
        pointsInTime = new List<PointInTime>();
        rb.GetComponent<Rigidbody>();
    }
 
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.R))
            StartRewinding();
        //if (player.whiteball.velocity == Vector3.zero && player.yellowball.velocity == Vector3.zero && player.redball.velocity == Vector3.zero)
        if(player.whiteball.velocity == Vector3.zero && player.yellowball.velocity == Vector3.zero && player.redball.velocity == Vector3.zero)
        {

            StopRewinding();


        }
    }
    void FixedUpdate()
    {
        if (isRewinding)
        {
            Rewind();
        }
        else
            Record();
    }
    void Rewind()
    {
        if (pointsInTime.Count > 0)
        {
            PointInTime pointInTime = pointsInTime[0];
            transform.position = pointInTime.position;
            transform.rotation = pointInTime.rotation;
            this.pointsInTime.RemoveAt(0);
        }
        else
        {

            StopRewinding();
        }
    }
    void Record()
    {
        pointsInTime.Insert(0, new PointInTime(transform.position, transform.rotation));
    }
    // Update is called once per frame
    void StartRewinding()
    {
      
       // GetComponent<AudioSource>().PlayOneShot(ReplayVoice);
    
        isRewinding = true;
        rb.isKinematic = true;
    }
    void StopRewinding()
    {
        isRewinding = false;
        rb.isKinematic = false;
     
    }
}
