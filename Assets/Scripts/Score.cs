﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour {
   
    public AudioClip hitTheBall;
    public bool hitRedBall = false;
    public bool hitYellowBall = false;
    public int score;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Red")
        {
            GetComponent<AudioSource>().PlayOneShot(hitTheBall);
            hitRedBall = true;
        }
        if (collision.gameObject.tag == "Yellow")
        {
            GetComponent<AudioSource>().PlayOneShot(hitTheBall);
            hitYellowBall = true;
        }

        
    }

  

}
