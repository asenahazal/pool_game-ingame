﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour, IForce
{
    public Rigidbody whiteball;
    public Rigidbody yellowball;
    public Rigidbody redball;

    public Text ScoreText;
    public Text YouWin;
    public Text strokeNumber;
    public int vurusSayisi;
    public bool rewind = false;

    GUIStyle font;

    private bool onGround;
    private float hitPressure;
    private float minHit;
    private float maxHitPressure;

    public bool testRewind;
    public AudioClip CUE;

    void Start()
    {
    


        onGround = true;
        hitPressure = 0f;
        minHit = 5f;
        maxHitPressure = 35f;

        font = new GUIStyle();
        font.fontSize = 35;
        GetComponent<Score>().score = 0;
        SetScoreText();
        YouWin.text = "";
        strokeNumber.text = "";

    }

    void OnGUI()
    {

        if (GetComponent<Replay>().isRewinding) { GUI.Label(new Rect(500, 500, 500, 20), "Atış geri alınıyor bekleyin..", font); }
        else
        {

            if (whiteball.velocity == Vector3.zero && yellowball.velocity == Vector3.zero && redball.velocity == Vector3.zero)
            {
                
                GUI.Label(new Rect(100, 500, 500, 20), "Power : " + hitPressure, font);
                GUI.Label(new Rect(500, 500, 500, 20), "Atışa Hazır. Skor Güncellendi..", font);


                if (GetComponent<Score>().hitRedBall == true && GetComponent<Score>().hitYellowBall == true)
                {

                    GetComponent<Score>().score += 1;
                    //skoru tut
                    PlayerPrefs.SetString("LastScore", "LastScore");
                    PlayerPrefs.SetInt("SaveScore", GetComponent<Score>().score);
                    PlayerPrefs.Save();

                    GetComponent<Score>().hitYellowBall = false;
                    GetComponent<Score>().hitRedBall = false;

                    SetScoreText();

                    if (GetComponent<Score>().score >= 5)
                    {
                        YouWinText();
                        Invoke("GoMainMenu", 5);
                    }

                }
            }


            else { GUI.Label(new Rect(500, 500, 500, 20), "Toplar hareket halinde.", font); }
        }
    }
    void Update()
    {

        if (whiteball.velocity == Vector3.zero && yellowball.velocity == Vector3.zero && redball.velocity == Vector3.zero)
        {
     
            onGround = true;
       
        }
        if (onGround)
        {
            if (Input.GetButton("Jump"))
            {
                if (hitPressure < maxHitPressure)
                {
                    hitPressure += Time.deltaTime * 10;

                }
                else
                {
                    hitPressure = maxHitPressure;
                }
            }
            else
            {
                if (hitPressure > 0f )
                {
                    GetComponent<AudioSource>().PlayOneShot(CUE);
                    hitPressure = hitPressure + minHit;
                    whiteball.velocity = transform.TransformDirection(Vector3.forward * hitPressure);
                    whiteball.name = "Beyaz";//nesne ismi
                    vurusSayisi += 1;
                    vurusayisi();
                    PlayerPrefs.SetString("LastStroke", "LastStroke");
                    PlayerPrefs.SetInt("SaveStroke", vurusSayisi);
                    PlayerPrefs.Save();
                    hitPressure = 0f;
                    onGround = false;
                   


                }
            }
        }




    }
    void GoMainMenu()
    {
        SceneManager.LoadScene("Menu");
    }
    void vurusayisi()
    {
        strokeNumber.text = "Stroke Number:" + vurusSayisi.ToString();
    }
    void SetScoreText()
    {
        ScoreText.text = "Score: " + GetComponent<Score>().score.ToString();
    }
    void YouWinText()
    {
        YouWin.text = "YOU WIN";
    }


}