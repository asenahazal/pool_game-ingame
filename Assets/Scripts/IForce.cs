﻿public interface IForce
{
    bool Equals(object other);
    int GetHashCode();
    string ToString();
}