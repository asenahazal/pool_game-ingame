﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SCOREBOARD : MonoBehaviour
{
    public PlayerController play;


    void OnGUI()
    {
        if (PlayerPrefs.HasKey("LastScore"))
        {

            GUI.Label(new Rect(500, 100, 500, 20), PlayerPrefs.GetString("LastScore") + ":" + PlayerPrefs.GetInt("SaveScore"));
        }
        else
        {
            GUI.Label(new Rect(500, 100, 500, 20), "No scores Yet!");
        }
       

        if (PlayerPrefs.HasKey("LastStroke"))
        {
            GUI.Label(new Rect(500, 220, 500, 20), PlayerPrefs.GetString("LastStroke") + ":" + PlayerPrefs.GetInt("SaveStroke"));
        }
        else
        {
            GUI.Label(new Rect(500, 220, 500, 20), "No strokes Yet!");
        }
     
        if (PlayerPrefs.HasKey("LastTime"))
        {
            GUI.Label(new Rect(500, 220, 500, 20), PlayerPrefs.GetString("LastTime") + ":" + PlayerPrefs.GetFloat("SaveTheTime"));
        }

    }
}